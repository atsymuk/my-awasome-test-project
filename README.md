Cubits Homepage POC

## Prerequisites

* Node v8+

## Installation

Dependencies:

```
$ npm install
```

## Usage

Start the application:

```bash
$ npm start
```

Prepare a build:

```bash
$ npm run build
```
