const path = require('path');
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

global.wndow = {};
global.FormData = class FormData { append() { } };
global.URL = function (endpoint) { this.hostname = endpoint.replace(/^https?:\/\//, '').split(':')[0]; }

Enzyme.configure({ adapter: new Adapter() });
global.Enzyme = Enzyme;
global.shallow = Enzyme.shallow;
global.React = React;
